"""Homework 2-3 | Client using TCP."""

import socket


HOST = '127.0.0.1'
PORT = 2024
BUFFER_SIZE = 1024


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
    sock.bind((HOST, PORT))
    sock.listen()
    print('Server started')
    conn, address = sock.accept()
    print('Connected by', address)
    while True:
        data = conn.recv(BUFFER_SIZE)
        if not data:
            break
        else:
            numbers = data.decode()
            print(f"Received {numbers}")
            the_sum = sum(map(int, numbers.split(',')))
            conn.send(str(the_sum).encode())
            print(f"Sent the result: {the_sum}")
