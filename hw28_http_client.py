"""Homework 2-8 | HTTP client."""

import json
import socket
from typing import Optional


URL = 'https://jsonplaceholder.typicode.com/todos/'


def prepare_url(url: str) -> tuple[str, str]:
    """Prepare the URL for sending HTTP requests."""
    url = url.rstrip('/')
    if url.startswith('http'):
        url = url.split('://')[1]
    url_split = url.split('/')
    return url_split[0], '/'.join(url_split[1:])


def prepare_data(data: Optional[dict]) -> str:
    """Prepare the data for sending HTTP requests."""
    if data:
        return json.dumps(data)
    else:
        return ''


def prepare_request_message(
        domain: str,
        parameters: str,
        data: Optional[dict],
        method: str
) -> bytes:
    """Prepare the HTTP request message."""
    request_line = f"{method} /{parameters} HTTP/1.0\r\n"
    body = prepare_data(data)
    header = (f"Host: {domain}\r\n"
              f"Content-Type: application/json; charset=utf-8\r\n"
              f"Content-Length: {len(body)}\r\n"
              f"\r\n")
    request_message = request_line + header + body
    print(f'Request:\n{request_message}')

    return request_message.encode('utf-8')


def make_http_request(
        url: str,
        port: int = 80,
        buffer_size: int = 1024,
        data: Optional[dict] = None,
        method: str = 'GET'
) -> str:
    """Make HTTP request."""

    domain, parameters = prepare_url(url)
    request_message = prepare_request_message(domain, parameters, data, method)

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client_socket:
        address = (domain, port)
        client_socket.connect(address)
        print(f'Connected to {address}')

        client_socket.sendall(request_message)

        response = ''
        while True:
            buffer = client_socket.recv(buffer_size).decode('utf-8')
            if not buffer:
                break
            response += buffer

    return response


def main() -> None:
    """Send different types of HTTP requests to an API."""

    print('\n--------GET')
    print(make_http_request(URL))

    print('\n--------POST')
    todo_data = {
        'completed': False,
        'title': 'complete home task 2',
        'userId': 7
    }
    print(make_http_request(URL, data=todo_data, method='POST'))

    print('\n--------PUT')
    completed_data = {
        'completed': True
    }
    print(make_http_request(URL + '101', data=completed_data, method='PUT'))

    print('\n--------PATCH')
    title_data = {
        'title': 'sleep over night'
    }
    print(make_http_request(URL + '101', data=title_data, method='PATCH'))

    print('\n--------DELETE')
    print(make_http_request(URL + '101', method='DELETE'))


if __name__ == '__main__':
    main()
