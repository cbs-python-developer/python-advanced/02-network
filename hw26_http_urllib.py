"""Homework 2-6 | HTTP requests using urllib library."""

import json
import pprint
from typing import Optional
from urllib.parse import urlencode
from urllib.request import Request, urlopen


URL = 'https://jsonplaceholder.typicode.com/todos/'


def prepare_form_data(data: dict) -> bytes:
    """Prepare the data to be sent with an HTTP request.
    Content-Type: application/x-www-form-urlencoded."""
    return urlencode(data).encode('utf-8')


def prepare_json_data(data: dict) -> bytes:
    """Prepare the data to be sent with an HTTP request.
    Content-Type: application/json."""
    return json.dumps(data).encode('utf-8')


def make_request(
        url: str,
        data: Optional[bytes] = None,
        headers: dict = {},
        method: Optional[str] = None
) -> bytes:
    """Make an HTTP request."""
    request = Request(url, data=data, headers=headers, method=method)
    with urlopen(request) as response:
        print(response.status, response.reason)
        return response.read()


def main() -> None:
    """Send different types of HTTP requests to an API."""

    print('\n--------GET')
    with urlopen(URL + '/200') as get_response:
        print(get_response.status, get_response.reason)
        body = get_response.read()
    response_data = json.loads(body)
    pprint.pprint(response_data)

    print('\n--------POST')
    todo_data = {
        'completed': False,
        'title': 'complete home task 2',
        'userId': 7
    }
    post_data = prepare_form_data(todo_data)
    with urlopen(URL, data=post_data) as post_response:
        print(post_response.status, post_response.reason)
        body = post_response.read()
        print(body.decode("utf-8"))

    print('\n--------PUT')
    completed_data = {
        'completed': True
    }
    put_data = prepare_form_data(completed_data)
    put_response = make_request(URL + '/200', data=put_data, method='PUT')
    print(put_response.decode('utf-8'))

    print('\n--------PATCH')
    title_data = {
        'title': 'sleep over night'
    }
    patch_data = prepare_json_data(title_data)
    headers = {"Content-Type": "application/json"}
    patch_response = make_request(
        URL + '/200', data=patch_data, headers=headers, method='PATCH'
    )
    print(patch_response.decode('utf-8'))

    print('\n--------DELETE')
    delete_response = make_request(URL + '/200', method='DELETE')
    print(delete_response.decode('utf-8'))


if __name__ == '__main__':
    main()
