"""Homework 2-3 | Client using TCP."""

import socket


HOST = '127.0.0.1'
PORT = 2024
BUFFER_SIZE = 1024
NUMBERS = [1, 2, 3, 4, 5, 6, 7, 8, 9]


with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as sock:
    address = (HOST, PORT)
    sock.connect(address)
    print(f'Connected to {address}')
    numbers = str(NUMBERS)[1:-1]
    sock.sendall(numbers.encode())
    print(f'Sent {numbers} to {address}')
    data = sock.recv(BUFFER_SIZE)
    print(f"Received the result: {data.decode()}")
