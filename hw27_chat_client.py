"""Homework 2-7 | Chatroom client using TCP."""

import select
import socket
import sys

from hw27_chat_server import HOST, PORT, BUFFER_SIZE


def start_client() -> None:
    """Start the client."""

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as client:
        address = (HOST, PORT)
        client.connect(address)
        print(f'Connected to {address}')

        while True:
            input_streams = [sys.stdin, client]
            ready_to_read, ready_to_write, in_error = select.select(
                input_streams, [], []
            )
            for stream in ready_to_read:
                if stream == client:
                    message = stream.recv(BUFFER_SIZE)
                    if not message:
                        print('The Chat closed.')
                        exit()
                    print(message.decode('utf-8'))
                else:
                    message = sys.stdin.readline()
                    client.sendall(message.encode('utf-8'))
                    print(f"(You): {message}")


if __name__ == '__main__':
    start_client()
