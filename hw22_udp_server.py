"""Homework 2-2 | Server using UDP."""

import socket


HOST = '127.0.0.1'
PORT = 2024
BUFFER_SIZE = 1024


with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
    sock.bind((HOST, PORT))
    print('Server started')
    while True:
        clint_id, address = sock.recvfrom(BUFFER_SIZE)
        print(f"Received ID={clint_id.decode()} from {address}")
