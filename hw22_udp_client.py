"""Homework 2-2 | Client using UDP."""

import socket


HOST = '127.0.0.1'
PORT = 2024
CLIENT_ID = '12345678'


with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as sock:
    address = (HOST, PORT)
    sock.sendto(CLIENT_ID.encode(), address)
    print(f"Message sent to {address}")
