"""Homework 2-6 | HTTP requests using requests library."""

import requests


URL = 'https://jsonplaceholder.typicode.com/todos/'


def main() -> None:
    """Send different types of HTTP requests to an API."""

    print('\n--------GET')
    get_response = requests.get(URL + '/200')
    print(get_response.status_code, get_response.reason)
    print(get_response.text)
    print(get_response.headers['Date'])

    print('\n--------POST')
    todo_data = {
        'completed': False,
        'title': 'complete home task 2',
        'userId': 7
    }
    post_response = requests.post(URL, data=todo_data)
    print(post_response.status_code, post_response.reason)
    post_json = post_response.json()
    print(type(post_json), post_json)

    print('\n--------PUT')
    completed_data = {
        'completed': True
    }
    put_response = requests.put(URL + '/200', data=completed_data)
    print(put_response.status_code, put_response.reason)
    print(put_response.content.decode('utf-8'))

    print('\n--------PATCH')
    title_data = {
        'title': 'sleep over night'
    }
    patch_response = requests.patch(URL + '/200', data=title_data)
    print(patch_response.status_code, patch_response.reason)
    print(patch_response.text)

    print('\n--------DELETE')
    delete_response = requests.delete(URL + '/200')
    print(delete_response.status_code, delete_response.reason)
    print(delete_response.text)


if __name__ == '__main__':
    main()
