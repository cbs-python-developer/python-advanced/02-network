"""Homework 2-7 | Chatroom server using TCP."""

import socket
import _thread


HOST = '127.0.0.1'
PORT = 56000
BUFFER_SIZE = 2048
NUMBER_OF_ACTIVE_CONNECTIONS = 100


def broadcast(message: str, connection: socket.socket) -> None:
    """Broadcast the message to all the clients,
    except the one sending the message."""

    for client in chat_clients:
        if client != connection:
            client.sendall(message.encode('utf-8'))


def get_chat_members(members: list[socket.socket]) -> str:
    """Get all the members of the Chatroom."""

    members_str = '\n'.join(['\t' + str(client.getpeername()) for client in members])
    return "In the chat:\n" + members_str


def client_thread(connection: socket.socket, address: tuple[str, int]) -> None:
    """Interact with a client."""

    connection.sendall("Welcome to the Chat!".encode('utf-8'))
    broadcast(f"{address}: connected to the Chat", connection)

    while True:
        message = connection.recv(BUFFER_SIZE).decode('utf-8')
        if message:
            message_to_send = f"{address}: {message}"
            print(message_to_send)
            broadcast(message_to_send, connection)
        else:
            message_to_send = f"{address}: disconnected"
            print(message_to_send)
            broadcast(message_to_send, connection)
            chat_clients.remove(connection)
            print(get_chat_members(chat_clients))
            connection.close()
            _thread.exit()


def start_server() -> None:
    """Start the server."""

    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as server:
        server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server.bind((HOST, PORT))
        server.listen(NUMBER_OF_ACTIVE_CONNECTIONS)
        print('The Chat Server started')

        while True:
            conn, addr = server.accept()
            print(f"Connected by {addr}")

            chat_clients.append(conn)
            print(get_chat_members(chat_clients))

            # create an individual thread for every client that connects
            _thread.start_new_thread(client_thread, (conn, addr))


if __name__ == '__main__':

    chat_clients: list[socket.socket] = []
    start_server()
